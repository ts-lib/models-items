<?php

namespace TsLib\ModelsItems;


use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

function ucsentence($str) {
  if ($str) { // input
    $str = preg_replace('/'.chr(32).chr(32).'+/', chr(32), $str); // recursively replaces all double spaces with a space
    if (($x = substr($str, 0, 10)) && ($x == strtoupper($x))) $str = strtolower($str); // sample of first 10 chars is ALLCAPS so convert $str to lowercase; if always done then any proper capitals would be lost
    $na = array('. ', '! ', '? ', '- '); // punctuation needles
    foreach ($na as $n) { // each punctuation needle
      if (strpos($str, $n) !== false) { // punctuation needle found
        $sa = explode($n, $str); // split
        foreach ($sa as $s) $ca[] = ucfirst($s); // capitalize
        $str = implode($n, $ca); // replace $str with rebuilt version
        unset($ca); //  clear for next loop
      }
    }
    return ucfirst(trim($str)); // capitalize first letter in case no punctuation needles found
  }
}

class Item extends Model
{
    use SearchableTrait;

    protected $table = "items";

    protected $searchable = [
        'columns' => [
            'description'   => 20,
            'model'         => 20,
            'brands.name'   => 14,
            'code'          => 14,
            'keywords'      => 12,
            'benchmark_s'   => 5,
            'benchmark_t'   => 5,
            'benchmark_o'   => 5,
            'description_1' => 5
            /*
            'description'   => 25,
            'brands.name'   => 20,
            'code'          => 20,
            'model'         => 15,
            'keywords'      => 10,
            'description_1' => 5
            */
        ],
        'joins' => [
            'brands' => ['brands.id','items.brand_id'],
        ],
    ];

    public $dates = [
        'received_date'
    ];

    public function getImageThumbnailAttribute()
    {
        $filepath = public_path(vsprintf('thumbs'.DIRECTORY_SEPARATOR.'items'.DIRECTORY_SEPARATOR.'%s.jpg', [md5('tsimg'.$this->id)]));
        $thumbpath = url(vsprintf('thumbs/items/%s.jpg', [md5('tsimg'.$this->id)]));

        if(is_file($filepath))
            return $thumbpath;
        else
            return $this->image1;
    }

    public function scopeExpress($query)
    {
        return $query->where('shop_express','1');
    }

    public function scopePromo($query)
    {
        return $query->where('id_promo','<>','');
    }

    // le llamo Activeall para que no reemplace el campo Active de la tabla
    public function scopeActiveall($query)
    {
        return $query->where('active', true)->where('isonline', true);
    }
/*
    public function brand()
    {
        return $this->belongsTo('App\Brand');
    }

    public function inventories()
    {
        return $this->hasMany('App\Inventory');
    }

    public function prices()
    {
        return $this->hasMany('App\Price');
    }

    public function direct()
    {
        return $this->hasMany('App\ItemDirect');
    }

    public function relateds()
    {
        return $this->hasMany('App\ItemRelated');
    }

    public function correlateds()
    {
        return $this->hasMany('App\ItemCorrelated');
    }

    public function members()
    {
        return $this->hasMany('App\ItemMember');
    }

    public function subcategory()
    {
        return $this->hasMany('App\ItemSubcategory');
    }

    public function subcategories()
    {
        return $this->belongsToMany('App\Subcategory', 'items_subcategories');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Category', 'items_subcategories');
    }

    public function lists()
    {
        return $this->belongsToMany('App\Lists', 'list_detail', 'item_id', 'list_id');
    }

    public function quoteEnduserDetail()
    {
        return $this->belongsToMany('App\QuoteEnduserDetail', 'items_id', 'id');
    }

    public function thumbnails()
    {
        return $this->hasMany('App\Thumbnail', 'entity_id')->where('type', 'item');
    }

    private function getImageThumbnail($size, $format = 'jpg')
    {
        if(count($this->thumbnails) > 0 && count($thumb = $this->thumbnails->where('size', $size)->where('format', $format)) > 0)
        {
            return 'data:image/'.$thumb->first()->format.';base64,'.$thumb->first()->data;
        }
        else
        {
            return $this->image1;
        }
    }

    public function getImageThumbnailSmallAttribute()
    {
        return $this->getImageThumbnail('small');
    }

    public function getImageThumbnailMediumAttribute()
    {
        //\Illuminate\Support\Facades\Log::info('med '.$this->id);
        return $this->getImageThumbnail('medium');
    }
*/
    public function getMetakeywordsAttribute()
    {
        if($this->keywords != '')
            return implode(",", [$this->code_complet, $this->model, $this->brand->name, $this->keywords]);
        else
            return implode(",", [$this->code_complet, $this->model, $this->brand->name]);
    }

    public function getMetadescAttribute()
    {
        return $this->brand->name .' '. strtoupper($this->model) .' '. ucsentence($this->description) .' '. strtoupper($this->code_complet) .' Tecnosinergia';
    }

    public function getDescriptionUcAttribute()
    {
        return ucsentence($this->description);
    }

    public function getTypeAttribute($value)
    {
        //9038 = Seguro de Envio, 11368 = Descuento de Pago en Linea
        if($this->id == '9038' || $this->id == '11368' ){
            return 1;
        }

        return $value;
    }
}
