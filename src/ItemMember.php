<?php

namespace TsLib\ModelsItems;

use Illuminate\Database\Eloquent\Model;

class ItemMember extends Model
{
    protected $table = "items_member";

    public function item()
    {
        return $this->belongsTo('TsLib\ModelsItems\Item', 'item_member');
    }
}
